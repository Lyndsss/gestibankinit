package com.gestibank.models;

import java.util.Date;


public class Transaction {
	
	/* ATTRIBUTS DE CLASSE */
		//Attributs li�s aux colonnes de BDD
		private int id;
		private TypeTransaction typeTransaction;
		private CompteBancaire compte;
		private double montant;
		private Date dateTransaction;
		private TypeOperation typeOperation;
		
		
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public Transaction() {
			// TODO Auto-generated constructor stub
		}
		
		
	/* GETERS-SETERS */
		//id
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		
		//compte
		public CompteBancaire getCompte() {
			return compte;
		}
		public void setCompte(CompteBancaire compte) {
			this.compte = compte;
		}
		
		//montant
		public double getMontant() {
			return montant;
		}
		public void setMontant(double montant) {
			this.montant = montant;
		}
		
		//typeOperation
		public TypeOperation getTypeOperation() {
			return typeOperation;
		}
		public void setTypeOperation(TypeOperation typeOperation) {
			this.typeOperation = typeOperation;
		}
		
		//typeTransaction
		public TypeTransaction getTypeTransaction() {
			return typeTransaction;
		}
		public void setTypeTransaction(TypeTransaction typeTransaction) {
			this.typeTransaction = typeTransaction;
		}
		
		//dateTransaction
		public Date getDateTransaction() {
			return dateTransaction;
		}
		public void setDateTransaction(Date dateTransaction) {
			this.dateTransaction = dateTransaction;
		}
}
