package com.gestibank.dao.enums.interfaces;

import java.util.ArrayList;

import com.gestibank.models.StatutAffectation;


public interface StatutAffectationDAO {
	StatutAffectation readStatutAffectation(int id);
	ArrayList<StatutAffectation> readAllStatutAffectations();
}
