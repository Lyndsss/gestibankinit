package com.gestibank.models;

import java.util.Date;


public class DemandeChequier {
	
	/* ATTRIBUTS DE CLASSE */
		//Attributs li�s aux colonnes de BDD
		private String ribCompte;
		private StatutDemandeChequier statut;
		private Date dateDemande;
		private Date dateTraitement;
		
		
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public DemandeChequier() {
			// TODO Auto-generated constructor stub
		}
		
		
	/* GETERS-SETERS */
		//dateDemande
		public Date getDateDemande() {
			return dateDemande;
		}
		public void setDateDemande(Date dateDemande) {
			this.dateDemande = dateDemande;
		}
		
		//dateTraitement
		public Date getDateTraitement() {
			return dateTraitement;
		}
		public void setDateTraitement(Date dateTraitement) {
			this.dateTraitement = dateTraitement;
		}
		
		//ribCompte
		public String getRibCompte() {
			return ribCompte;
		}
		public void setRibCompte(String ribCompte) {
			this.ribCompte = ribCompte;
		}
		
		//statut
		public StatutDemandeChequier getStatut() {
			return statut;
		}
		public void setStatut(StatutDemandeChequier statut) {
			this.statut = statut;
		}
}
