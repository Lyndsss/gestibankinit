package com.gestibank.models;

public abstract class Utilisateur {
	
	/* ATTRIBUTS DE CLASSE */
		//Attributs li�s aux colonnes de BDD
		protected String loginUser;
		protected String password;
		protected String nom;
		protected String prenom;
		protected String mail;
		protected String tel;
	
	
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		protected Utilisateur() {
			// TODO Auto-generated constructor stub
		}
	
	
	/* GETERS-SETERS */
		//loginUser
		public String getLoginUser() {
			return loginUser;
		}
		public void setLoginUser(String loginUser) {
			this.loginUser = loginUser;
		}
		
		//password
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		
		//nom
		public String getNom() {
			return nom;
		}
		public void setNom(String nom) {
			this.nom = nom;
		}
		
		//prenom
		public String getPrenom() {
			return prenom;
		}
		public void setPrenom(String prenom) {
			this.prenom = prenom;
		}
		
		//mail
		public String getMail() {
			return mail;
		}
		public void setMail(String mail) {
			this.mail = mail;
		}
		
		//tel
		public String getTel() {
			return tel;
		}
		public void setTel(String tel) {
			this.tel = tel;
		}
	
}
