package com.gestibank.dao.interfaces;

import java.util.ArrayList;

import com.gestibank.models.DemandeChequier;


public interface DemandeChequierDAO {
	Boolean createDemandeChequier(DemandeChequier chequier);
	DemandeChequier readDemandeChequierByRibCompte(String rib);
	Boolean updateDemandeChequierByRibCompte(DemandeChequier chequier);
	Boolean deleteDemandeChequierByRibCompte(String rib);
	ArrayList<DemandeChequier> readAllDemandeChequier();
}