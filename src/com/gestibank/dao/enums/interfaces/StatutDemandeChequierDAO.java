package com.gestibank.dao.enums.interfaces;

import java.util.ArrayList;

import com.gestibank.models.StatutDemandeChequier;


public interface StatutDemandeChequierDAO {
	StatutDemandeChequier readStatutDemandeChequier(int id);
	ArrayList<StatutDemandeChequier> readAllStatutDemandeChequiers();
}