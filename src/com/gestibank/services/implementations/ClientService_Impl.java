package com.gestibank.services.implementations;

import java.util.ArrayList;
import java.util.Date;

import com.gestibank.models.Client;
import com.gestibank.models.CompteBancaire;
import com.gestibank.models.Transaction;
import com.gestibank.services.interfaces.ClientService;



public class ClientService_Impl extends UserService_Impl implements ClientService {
	
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public ClientService_Impl() {
			// TODO Auto-generated constructor stub
		}
	
	/* METHODES INTERPHASE */
		//Boolean verifCredentials(String login, String pwd);
		public Boolean updateCompteClient(Client client) {
			// TODO
			return false;
		}
		public ArrayList<CompteBancaire> readListComptesBancairesByLogin(String login) {
			// TODO
			return null;
		}
		public Boolean createTransactionByLogin(String login, Transaction transaction) {
			// TODO
			return false;
		}
		public Boolean createVirementByLogin(String login, Transaction transactionDebit, Transaction transactionCredit) {
			// TODO
			return false;
		}
		public ArrayList<Transaction> readListTransactionByLogin(String login, String rib, Date dateDebut, Date dateFin) {
			// TODO
			return null;
		}
		public Boolean printListTransaction(ArrayList<Transaction> listTransaction) {
			// TODO
			return false;
		}
		public Boolean createDemandeChequierByLogin(String login, String rib) {
			// TODO
			return false;
		}
}
