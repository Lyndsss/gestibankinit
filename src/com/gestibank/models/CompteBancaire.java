package com.gestibank.models;

import java.util.Date;

import java.util.ArrayList;

public class CompteBancaire {
	
	/* ATTRIBUTS DE CLASSE */
		//Constantes
		private final double PLAFOND_REMUNERATION = 1000;
		private final double TAUX_INTERET = 0.18;
		private final int NB_JOUR_DEBIT = 90;
		private final double TAUX_REMUNERATION = 0.02;
		private final int NB_JOUR_DEPOT = 365;
	
		//Attributs li�s aux colonnes de BDD
		private String rib;
		private TypeCompte typeCompte;
		private Date dateCreation;
		private double decouvert;
		private double plafondDecouvert;
		private ArrayList<Transaction> listeTransactions;
		private Client propritaireClient;
		private double montantVariableRemunerationInteret;
		private int nbJoursMontantVariable;
	
	
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public CompteBancaire() {
			// TODO Auto-generated constructor stub
		}
		

	/* GETERS CONSTANTES */
		//NB_JOUR_DEBIT
		public int getNB_JOUR_DEBIT() {
			return NB_JOUR_DEBIT;
		}
		
		//NB_JOUR_DEPOT
		public int getNB_JOUR_DEPOT() {
			return NB_JOUR_DEPOT;
		}
		
		//PLAFOND_REMUNERATION
		public double getPLAFOND_REMUNERATION() {
			return PLAFOND_REMUNERATION;
		}
		
		//TAUX_INTERET
		public double getTAUX_INTERET() {
			return TAUX_INTERET;
		}
		
		//TAUX_REMUNERATION
		public double getTAUX_REMUNERATION() {
			return TAUX_REMUNERATION;
		}
		
	/* GETERS-SETERS */
		//dateCreation
		public Date getDateCreation() {
			return dateCreation;
		}
		public void setDateCreation(Date dateCreation) {
			this.dateCreation = dateCreation;
		}
		
		//decouvert
		public double getDecouvert() {
			return decouvert;
		}
		public void setDecouvert(double decouvert) {
			this.decouvert = decouvert;
		}
		
		//listeTransactions
		public ArrayList<Transaction> getListeTransactions() {
			return listeTransactions;
		}
		public void setListeTransactions(ArrayList<Transaction> listeTransactions) {
			this.listeTransactions = listeTransactions;
		}
		
		//plafondDecouvert
		public double getPlafondDecouvert() {
			return plafondDecouvert;
		}
		public void setPlafondDecouvert(double plafondDecouvert) {
			this.plafondDecouvert = plafondDecouvert;
		}
		
		//propritaireClient
		public Client getPropritaireClient() {
			return propritaireClient;
		}
		public void setPropritaireClient(Client propritaireClient) {
			this.propritaireClient = propritaireClient;
		}
		
		//rib
		public String getRib() {
			return rib;
		}
		public void setRib(String rib) {
			this.rib = rib;
		}
		
		//typeCompte
		public TypeCompte getTypeCompte() {
			return typeCompte;
		}
		public void setTypeCompte(TypeCompte typeCompte) {
			this.typeCompte = typeCompte;
		}
		
		//montantVariableRemunerationInteret
		public double getMontantVariableRemunerationInteret() {
			return montantVariableRemunerationInteret;
		}
		public void setMontantVariableRemunerationInteret(double montantVariableRemunerationInteret) {
			this.montantVariableRemunerationInteret = montantVariableRemunerationInteret;
		}
		
		//nbJoursMontantVariable
		public int getNbJoursMontantVariable() {
			return nbJoursMontantVariable;
		}
		public void setNbJoursMontantVariable(int nbJoursMontantVariable) {
			this.nbJoursMontantVariable = nbJoursMontantVariable;
		}
}
