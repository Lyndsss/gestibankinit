package com.gestibank.models;

import java.util.ArrayList;

public class Administrateur extends Utilisateur {
	
	/* ATTRIBUTS DE CLASSE */
		//Attributs li�s aux colonnes de BDD
		public String login;
		public String password;
		public ArrayList<Agent> listeAgents;
		
		
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public Administrateur() {
			// TODO Auto-generated constructor stub
		}
		
		
	/* GETERS-SETERS */
		//listeAgents
		public ArrayList<Agent> getListeAgents() {
			return listeAgents;
		}
		public void setListeAgents(ArrayList<Agent> listeAgents) {
			this.listeAgents = listeAgents;
		}
		
		//login
		public String getLogin() {
			return login;
		}
		public void setLogin(String login) {
			this.login = login;
		}
		
		//password
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
}
