package com.gestibank.services.interfaces;

import java.util.ArrayList;

import com.gestibank.models.Affectation;
import com.gestibank.models.Client;
import com.gestibank.models.CompteBancaire;
import com.gestibank.models.Transaction;



public interface AgentService {
	//Boolean verifCredentials(String login, String pwd);
	ArrayList<Client> readListClientsByMatricule(String matricule, String login_rib_nom);
	Boolean updateClientByMatricule(String matricule, String login);
	Boolean deleteClientByMatricule(String matricule, String login);
	Boolean printListClients(ArrayList<Client> listClient);
	Boolean createCompteBancaireByMatricule(String matricule, CompteBancaire compte);
	Boolean deleteCompteBancaireByMatricule(String matricule, String rib);
	ArrayList<CompteBancaire> readListComptesBancaireByMatricule(String matricule, String login);
	Boolean createTransactionByMatricule(String matricule, String login, Transaction transaction);
	Boolean createVirementByMatricule(String matrice, String login, Transaction transactionDebit, Transaction transactionCredit);
	ArrayList<Affectation> readListPendingAffectationsByMatricule(String matricule);
	Boolean updateAffectationByMatricule(String matricule, Affectation affectation, Boolean validation);
}
