package com.gestibank.dao.interfaces;

import java.util.ArrayList;

import com.gestibank.models.Client;


public interface ClientDAO {
	Boolean createClient(Client client);
	Client readClientByLogin(String login);
	Boolean updateClient(Client client);
	Boolean deleteClientByLogin(String login);
	ArrayList<Client> readAllClients();
	ArrayList<Client> readAllClientsByMatricule(String matricule);
	ArrayList<Client> readAllClientsByMatriculeandFiltre(String matricule, String loginClient_rib_nom);
}
