package com.gestibank.services.interfaces;

import com.gestibank.models.Affectation;

public interface GuestService {
	Boolean createAffectation(Affectation affectation);
}
