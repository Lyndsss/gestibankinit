package com.gestibank.services.interfaces;

import java.util.ArrayList;
import java.util.Date;

import com.gestibank.models.Client;
import com.gestibank.models.CompteBancaire;
import com.gestibank.models.Transaction;



public interface ClientService {
	//Boolean verifCredentials(String login, String pwd);
	Boolean updateCompteClient(Client client);
	ArrayList<CompteBancaire> readListComptesBancairesByLogin(String login);
	Boolean createTransactionByLogin(String login, Transaction transaction);
	Boolean createVirementByLogin(String login, Transaction transactionDebit, Transaction transactionCredit);
	ArrayList<Transaction> readListTransactionByLogin(String login, String rib, Date dateDebut, Date dateFin);
	Boolean printListTransaction(ArrayList<Transaction> listTransaction);
	Boolean createDemandeChequierByLogin(String login, String rib);
}
