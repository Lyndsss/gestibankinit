package com.gestibank.models;

/*
 * en_attente
 * en_cours
 * traite
 */

public class StatutAffectation extends TypeLibelle {
	
		
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public StatutAffectation() {
			// TODO Auto-generated constructor stub
		}
		
		
	/* GETERS-SETERS LABEL */
		public void setLibelle(String libelle) {
			String[] listeLibelles ={	"en_attente",
										"en_cours",
										"traite"		};
			super.setLibelles(libelle, listeLibelles);
		}
}
