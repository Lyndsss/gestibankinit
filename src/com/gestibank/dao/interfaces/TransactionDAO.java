package com.gestibank.dao.interfaces;

import java.util.ArrayList;

import com.gestibank.models.Transaction;


public interface TransactionDAO {
	Boolean createTransaction(Transaction transaction);
	Transaction readTransactionById(int id);
	Boolean updateTransaction(Transaction transaction);
	Boolean deleteTransaction(int id);
	ArrayList<Transaction> readAllTransaction();
	ArrayList<Transaction> readAllTransactionByRib(String rib);
}