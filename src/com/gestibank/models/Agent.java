package com.gestibank.models;

import java.util.ArrayList;
import java.util.Date;

public class Agent {
	
	/* ATTRIBUTS DE CLASSE */
		//Attributs li�s aux colonnes de BDD
		private Date dateDebutContrat;
		private ArrayList<Client> listeClients;
		private ArrayList<Affectation> listeAffectations;
		
		
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public Agent() {
			super();
			// TODO Auto-generated constructor stub
		}
	
	
	/* GETERS-SETERS */
		//dateDebutContrat
		public Date getDateDebutContrat() {
			return dateDebutContrat;
		}
		public void setDateDebutContrat(Date dateDebutContrat) {
			this.dateDebutContrat = dateDebutContrat;
		}
		
		//listeClients
		public ArrayList<Client> getListeClients() {
			return listeClients;
		}
		public void setListeClients(ArrayList<Client> listeClients) {
			this.listeClients = listeClients;
		}

		//listeAffectations
		public ArrayList<Affectation> getListeAffectations() {
			return listeAffectations;
		}


		public void setListeAffectations(ArrayList<Affectation> listeAffectations) {
			this.listeAffectations = listeAffectations;
		}
}
