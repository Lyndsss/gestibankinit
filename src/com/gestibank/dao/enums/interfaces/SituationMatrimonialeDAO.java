package com.gestibank.dao.enums.interfaces;

import java.util.ArrayList;

import com.gestibank.models.SituationMatrimoniale;


public interface SituationMatrimonialeDAO {
	SituationMatrimoniale readSituationMatrimoniale(int id);
	ArrayList<SituationMatrimoniale> readAllSituationMatrimoniales();
}
