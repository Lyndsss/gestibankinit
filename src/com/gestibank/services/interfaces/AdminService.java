package com.gestibank.services.interfaces;

import java.util.ArrayList;
import java.util.Date;

import com.gestibank.models.Affectation;
import com.gestibank.models.Agent;
import com.gestibank.models.Client;
import com.gestibank.models.StatutAffectation;



public interface AdminService {
	//Boolean verifCredentials(String login, String pwd);
	Boolean createAgentByLogin(String login, Agent agent);
	ArrayList<Agent> readListAgentByLogin(String login, String matricule_nom);
	Boolean updateAgentByLogin(String login, Agent agent);
	Boolean deleteAgentByLogin(String login, String matricule);
	ArrayList<Client> readListClientByLogin(String login, String matricule);
	ArrayList<Client> readListClientsByLogin(String loginAdmin, String matricule, String loginClient_rib_nom);
	Boolean updateClientByLogin(String loginAdmin, Client client);
	Boolean printListAgents(ArrayList<Client> listAgent);
	Boolean printListClients(ArrayList<Client> listClient);
	ArrayList<Affectation> readPendingAffectationsByLogin(String login);
	Boolean updateAffectationByLogin(String login, Affectation affectation);
	ArrayList<Affectation> readListAffectationsByLogin(String login);
	ArrayList<Affectation> readListAffectationsByLogin(String login, StatutAffectation statutAffectation);
	ArrayList<Affectation> readListAffectationsByLogin(String login, Date dateDemande_Affectation);
	ArrayList<Affectation> readListAffectationsByLogin(String login, StatutAffectation statutAffectation, Date dateDemande_Affectation);
}
