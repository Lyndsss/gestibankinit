package com.gestibank.models;

/*
 * compte_courant_avec_decouvert,
 * compte_courant_sans_decouvert,
 * compte_remunerateur
 */

public class TypeCompte extends TypeLibelle {
	
		
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public TypeCompte() {
			// TODO Auto-generated constructor stub
		}
		
		
	/* GETERS-SETERS LABEL */
		public void setLibelle(String libelle) {
			String[] listeLibelles ={	"compte_courant_avec_decouvert",
										"compte_courant_sans_decouvert",
										"compte_remunerateur"				};
			super.setLibelles(libelle, listeLibelles);
		}
}
