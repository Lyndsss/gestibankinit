package com.gestibank.dao.interfaces;

import java.util.ArrayList;

import com.gestibank.models.CompteBancaire;

public interface CompteBancaireDAO {
	Boolean createCompte(CompteBancaire compte);
	CompteBancaire readCompteByRib(String rib);
	Boolean updateCompte(CompteBancaire compte);
	Boolean deleteCompteByRib(String rib);
	ArrayList<CompteBancaire> readAllComptes();
	ArrayList<CompteBancaire> readAllComptesByLogin(String login);
}
