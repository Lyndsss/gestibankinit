package com.gestibank.services.interfaces;

public interface UserService {
	Boolean verifCredentials(String login, String pwd, String role);
	Boolean deconnection();
}
