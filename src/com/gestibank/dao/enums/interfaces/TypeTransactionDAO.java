package com.gestibank.dao.enums.interfaces;

import java.util.ArrayList;

import com.gestibank.models.TypeTransaction;

public interface TypeTransactionDAO {
	TypeTransaction readTypeTransaction(int id);
	ArrayList<TypeTransaction> readAllTypeTransactions();
}