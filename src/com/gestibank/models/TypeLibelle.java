package com.gestibank.models;

public abstract class TypeLibelle {
	
	/* ATTRIBUTS DE CLASSE */
		//Attributs li�s aux colonnes de BDD
		private String libelle;
	
		
	/* CONSTRUCTEURS */
		//Constructeur sans parametre
		protected TypeLibelle() {
			// TODO Auto-generated constructor stub
		}
		
	/* GETERS-SETERS LABEL */
		public String getLibelle() {
			return libelle;
		}
		protected void setLibelles(String libelle, String[] libelles_possibles) {
			boolean setPossible = false;
			for(String libelle_possible : libelles_possibles) {
				setPossible |= (libelle.equals(libelle_possible));
			}
			
			if(setPossible) { this.libelle = libelle; }
		}
}
