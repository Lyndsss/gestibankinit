package com.gestibank.dao.enums.interfaces;

import java.util.ArrayList;

import com.gestibank.models.TypeOperation;

public interface TypeOperationDAO {
	TypeOperation readTypeOperationById(int id);
	ArrayList<TypeOperation> readAllTypeOperations();
}
