package com.gestibank.dao.enums.interfaces;

import java.util.ArrayList;

import com.gestibank.models.TypeCompte;

public interface TypeCompteDAO {
	TypeCompte readTypeCompteById(int id);
	ArrayList<TypeCompte> readAllTypeComptes();
}
