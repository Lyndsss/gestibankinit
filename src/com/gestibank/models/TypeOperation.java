package com.gestibank.models;

/*
 * //Type Debit
 *	virement_vers,
 *	agios,
 *	paiement_carte,
 *	paiement_cheque,
 *	retrait,
 *	
 *	//Type Credit
 *	virement_depuis,
 *	remuneration,
 *	depot_liquide,
 *	depot_cheque,
 *	depot
 */

public class TypeOperation extends TypeLibelle {
	
		
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public TypeOperation() {
			// TODO Auto-generated constructor stub
		}
		
		
	/* GETERS-SETERS LABEL */
		public void setLibelle(String libelle) {
			String[] listeLibelles ={	"virement_vers",
										"agios",
										"paiement_carte",
										"paiement_cheque",
										"retrait",
										
										"virement_depuis",
										"remuneration",
										"depot_liquide",
										"depot_cheque",
										"depot"				};
			super.setLibelles(libelle, listeLibelles);
		}
}
