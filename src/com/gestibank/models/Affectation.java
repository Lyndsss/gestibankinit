package com.gestibank.models;

import java.util.Date;


public class Affectation {
	
	/* ATTRIBUTS DE CLASSE */
		//Attributs li�s aux colonnes de BDD
		private int id;
		private StatutAffectation statut;
		private Agent agentAffecte;
		private String login;
		private Date dateCreation;
		private Date dateAffectation;
		private String nom;
		private String prenom;
		private String mail;
		private String adresse;
		private String tel;
		private String raisonRefus;
		
		
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public Affectation() {
			// TODO Auto-generated constructor stub
		}
		
		
	/* GETERS-SETERS */
		//adresse
		public String getAdresse() {
			return adresse;
		}
		public void setAdresse(String adresse) {
			this.adresse = adresse;
		}
		
		//agentAffecte
		public Agent getAgentAffecte() {
			return agentAffecte;
		}
		public void setAgentAffecte(Agent agentAffecte) {
			this.agentAffecte = agentAffecte;
		}
		
		//dateAffectation
		public Date getDateAffectation() {
			return dateAffectation;
		}
		public void setDateAffectation(Date dateAffectation) {
			this.dateAffectation = dateAffectation;
		}
		
		//dateCreation
		public Date getDateCreation() {
			return dateCreation;
		}
		public void setDateCreation(Date dateCreation) {
			this.dateCreation = dateCreation;
		}
		
		//id
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		
		//login
		public String getLogin() {
			return login;
		}
		public void setLogin(String login) {
			this.login = login;
		}
		
		//mail
		public String getMail() {
			return mail;
		}
		public void setMail(String mail) {
			this.mail = mail;
		}
		
		//nom
		public String getNom() {
			return nom;
		}
		public void setNom(String nom) {
			this.nom = nom;
		}
		
		//prenom
		public String getPrenom() {
			return prenom;
		}
		public void setPrenom(String prenom) {
			this.prenom = prenom;
		}
		
		//statut
		public StatutAffectation getStatut() {
			return statut;
		}
		public void setStatut(StatutAffectation statut) {
			this.statut = statut;
		}
		
		//tel
		public String getTel() {
			return tel;
		}
		public void setTel(String tel) {
			this.tel = tel;
		}
		
		//message
		public String getMessage() {
			return raisonRefus;
		}
		public void setMessage(String raisonRefus) {
			this.raisonRefus = raisonRefus;
		}
}
