package com.gestibank.dao.interfaces;

import java.util.ArrayList;

import com.gestibank.models.Affectation;

public interface AffectationDAO {
	Boolean createAffectation(Affectation affectation);
	Affectation readAffectationById(int id);
	Boolean updateAffectation(Affectation affectation);
	Boolean deleteAffectationById(int id);
	ArrayList<Affectation> readAllAffectations();
	ArrayList<Affectation> readAllAffectationsByStatut(Affectation affectation);
}
