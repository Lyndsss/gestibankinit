package com.gestibank.models;

import java.util.ArrayList;
import java.util.Date;


public class Client {
	
	/* ATTRIBUTS DE CLASSE */
		//Attributs li�s aux colonnes de BDD
		private SituationMatrimoniale situation;
		private Date dateNaissance;
		private String adresse;
		private Date dateCreation;
		private ArrayList<CompteBancaire> comptesBancaires;
		private Agent agentAffecte;
		
		
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public Client() {
			super();
			// TODO Auto-generated constructor stub
		}
		
		
	/* GETERS-SETERS */
		//adresse
		public String getAdresse() {
			return adresse;
		}
		public void setAdresse(String adresse) {
			this.adresse = adresse;
		}
		
		//agentAffecte
		public Agent getAgentAffecte() {
			return agentAffecte;
		}
		public void setAgentAffecte(Agent agentAffecte) {
			this.agentAffecte = agentAffecte;
		}
		
		//comptesBancaires
		public ArrayList<CompteBancaire> getComptesBancaires() {
			return comptesBancaires;
		}
		public void setComptesBancaires(ArrayList<CompteBancaire> comptesBancaires) {
			this.comptesBancaires = comptesBancaires;
		}
		
		//dateCreation
		public Date getDateCreation() {
			return dateCreation;
		}
		public void setDateCreation(Date dateCreation) {
			this.dateCreation = dateCreation;
		}
		
		//dateNaissance
		public Date getDateNaissance() {
			return dateNaissance;
		}
		public void setDateNaissance(Date dateNaissance) {
			this.dateNaissance = dateNaissance;
		}
		
		//situation
		public SituationMatrimoniale getSituation() {
			return situation;
		}
		public void setSituation(SituationMatrimoniale situation) {
			this.situation = situation;
		}
}
