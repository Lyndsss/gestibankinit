package com.gestibank.dao.interfaces;

import com.gestibank.models.Administrateur;

public interface AdministrateurDAO {
	Boolean createAdministrateur(Administrateur administrateur);
	Administrateur readAdministrateurById(int id);
	Boolean updateAdministrateur(Administrateur administrateur);
	Boolean deleteAdministrateurById(int id);
}
