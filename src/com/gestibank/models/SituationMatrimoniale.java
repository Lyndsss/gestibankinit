package com.gestibank.models;

/*
 * celibataire,
 * marie, mariee,
 * pacse, pacsee,
 * concubinage,
 * union_libre,
 * veuf, veuve,
 * divorce, divorcee
 */

public class SituationMatrimoniale extends TypeLibelle {
	
		
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public SituationMatrimoniale() {
			// TODO Auto-generated constructor stub
		}
		
		
	/* GETERS-SETERS LABEL */
		public void setLibelle(String libelle) {
			String[] listeLibelles ={	"celibataire",
										"marie","mariee",
										"pacse","pacsee",
										"concubinage",
										"union_libre",
										"veuf","veuve",
										"divorce","divorcee"	};
			super.setLibelles(libelle, listeLibelles);
		}
}
