package com.gestibank.services.implementations;

import java.util.ArrayList;
import java.util.Date;

import com.gestibank.models.Affectation;
import com.gestibank.models.Agent;
import com.gestibank.models.Client;
import com.gestibank.models.StatutAffectation;
import com.gestibank.services.interfaces.AdminService;



public class AdminService_Impl extends UserService_Impl implements AdminService {
	
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public AdminService_Impl() {
			// TODO Auto-generated constructor stub
		}
	
	/* METHODES INTERPHASE */
		//Boolean verifCredentials(String login, String pwd);
		public Boolean createAgentByLogin(String login, Agent agent) {
			// TODO
			return false;
		}
		public ArrayList<Agent> readListAgentByLogin(String login, String matricule_nom) {
			// TODO
			return null;
		}
		public Boolean updateAgentByLogin(String login, Agent agent) {
			// TODO
			return false;
		}
		public Boolean deleteAgentByLogin(String login, String matricule) {
			// TODO
			return false;
		}
		public ArrayList<Client> readListClientByLogin(String login, String matricule) {
			// TODO
			return null;
		}
		public ArrayList<Client> readListClientsByLogin(String loginAdmin, String matricule, String loginClient_rib_nom) {
			// TODO
			return null;
		}
		public Boolean updateClientByLogin(String loginAdmin, Client client) {
			// TODO
			return false;
		}
		public Boolean printListAgents(ArrayList<Client> listAgent) {
			// TODO
			return false;
		}
		public Boolean printListClients(ArrayList<Client> listClient) {
			// TODO
			return false;
		}
		public ArrayList<Affectation> readPendingAffectationsByLogin(String login) {
			// TODO
			return null;
		}
		public Boolean updateAffectationByLogin(String login, Affectation affectation) {
			// TODO
			return false;
		}
		public ArrayList<Affectation> readListAffectationsByLogin(String login) {
			// TODO
			return null;
		}
		public ArrayList<Affectation> readListAffectationsByLogin(String login, StatutAffectation statutAffectation) {
			// TODO
			return null;
		}
		public ArrayList<Affectation> readListAffectationsByLogin(String login, Date dateDemande_Affectation) {
			// TODO
			return null;
		}
		public ArrayList<Affectation> readListAffectationsByLogin(String login, StatutAffectation statutAffectation, Date dateDemande_Affectation) {
			// TODO
			return null;
		}
}
