package com.gestibank.dao.interfaces;

import java.util.ArrayList;

import com.gestibank.models.Agent;

public interface AgentDAO {
	Boolean createAgent(Agent agent);
	Agent readAgentByMatricule(String matricule);
	Boolean updateAgent(Agent agent);
	Boolean deleteAgentByMatricule(String matricule);
	ArrayList<Agent> readAllAgents();
	ArrayList<Agent> readAllAgentsByLogin(String login);
	ArrayList<Agent> readAllAgentsByMatricule(String matricule);
}
