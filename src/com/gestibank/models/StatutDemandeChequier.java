package com.gestibank.models;

/*
 * en_attente,
 * accepte,
 * refuse
 */

public class StatutDemandeChequier extends TypeLibelle {
	
		
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public StatutDemandeChequier() {
			// TODO Auto-generated constructor stub
		}
		
		
	/* GETERS-SETERS LABEL */
		public void setLibelle(String libelle) {
			String[] listeLibelles ={	"en_attente",
										"accepte",
										"refuse"		};
			super.setLibelles(libelle, listeLibelles);
		}
}
