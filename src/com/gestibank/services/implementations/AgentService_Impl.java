package com.gestibank.services.implementations;

import java.util.ArrayList;

import com.gestibank.models.Affectation;
import com.gestibank.models.Client;
import com.gestibank.models.CompteBancaire;
import com.gestibank.models.Transaction;
import com.gestibank.services.interfaces.AgentService;



public class AgentService_Impl extends UserService_Impl implements AgentService {
	
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public AgentService_Impl() {
			// TODO Auto-generated constructor stub
		}
	
	/* METHODES INTERPHASE */
		//Boolean verifCredentials(String login, String pwd);
		public ArrayList<Client> readListClientsByMatricule(String matricule, String login_rib_nom) {
			// TODO
			return null;
		}
		public Boolean updateClientByMatricule(String matricule, String login) {
			// TODO
			return false;
		}
		public Boolean deleteClientByMatricule(String matricule, String login) {
			// TODO
			return false;
		}
		public Boolean printListClients(ArrayList<Client> listClient) {
			// TODO
			return false;
		}
		public Boolean createCompteBancaireByMatricule(String matricule, CompteBancaire compte) {
			// TODO
			return false;
		}
		public Boolean deleteCompteBancaireByMatricule(String matricule, String rib) {
			// TODO
			return false;
		}
		public ArrayList<CompteBancaire> readListComptesBancaireByMatricule(String matricule, String login) {
			// TODO
			return null;
		}
		public Boolean createTransactionByMatricule(String matricule, String login, Transaction transaction) {
			// TODO
			return false;
		}
		public Boolean createVirementByMatricule(String matrice, String login, Transaction transactionDebit, Transaction transactionCredit) {
			// TODO
			return false;
		}
		public ArrayList<Affectation> readListPendingAffectationsByMatricule(String matricule) {
			// TODO
			return null;
		}
		public Boolean updateAffectationByMatricule(String matricule, Affectation affectation, Boolean validation) {
			// TODO
			return false;
		}
}
